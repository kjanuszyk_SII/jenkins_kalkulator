pipeline {
    agent any
    parameters {
        choice choices: ['add', 'sub', 'div', 'mul'], description: 'Wybierz operację, jaką chcesz wykonać', name: 'Operacja'
        string defaultValue: '4', name: 'Pierwsza'
        string defaultValue: '2', name: 'Druga'
}
    triggers {
        pollSCM 'H/5 * * * *'
    }

    options {
        buildDiscarder logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '5', numToKeepStr: '20')
        timestamps()
        }
    stages {
        stage('Checkout') {
            steps {
            checkout([$class: 'GitSCM', branches: [[name: '*/main']], extensions: [], userRemoteConfigs: [[url: 'https://gitlab.com/kjanuszyk_SII/jenkins_kalkulator.git/']]])}
        }
        stage('run') {
            steps {
                bat 'python kalkulator.py ' + params.Operacja + ' ' + params.Pierwsza + ' ' + params.Druga
                echo 'Typ operacji: ' + params.Operacja
                echo 'Dane wejściowe: ' + params.Pierwsza + ' oraz ' + params.Druga
            }
        }
    }
    
}
