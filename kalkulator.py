import argparse


parser = argparse.ArgumentParser()
parser.add_argument('operation')
parser.add_argument('f_num')
parser.add_argument('s_num')
args = parser.parse_args()
if args.operation == "add":
    print(float(args.f_num) + float(args.s_num))
elif args.operation == "sub":
    print(float(args.f_num) - float(args.s_num))
elif args.operation == "div":
    print(float(args.f_num) / float(args.s_num))
elif args.operation == "mul":
    print(float(args.f_num) * float(args.s_num))